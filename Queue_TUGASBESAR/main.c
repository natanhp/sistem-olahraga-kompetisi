#include "header.h"

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {
    int menu;
    int add,edit,show,hapus;
    listParent list;
    // Inisialisasi list
    initList(&list);
    readDataJuara(&list);
    do
    {
    	system("color 0b");
    	printf("\033[H\033[J"); // Untuk clear screen di linux
    	system("CLS");
        printf("\n =======ASIAN GAMES======");
        printf("\n ");
        printf("\n [1]. TAMBAH DATA");
        printf("\n [2]. UBAH DATA");
        printf("\n [3]. TAMPIL DATA");
        printf("\n [4]. HAPUS DATA");
        printf("\n [5]. INPUT MEDALI");
        printf("\n [6]. JUARA UMUM");
        printf("\n [0]. KELUAR");
        printf("\n ========================");
        printf("\n Masukan Pilihan >> "); scanf("%d",&menu);
        printf("\n ========================");
        switch(menu)
        {
            case 1	:
                    do
					{
						system("color 0f");
                    	system("CLS");
                    	printf("\n Menu TAMBAH DATA");
	                    printf("\n =============");
	                    printf("\n [1].TAMBAH DATA CABANG OLAHRAGA");
	                    printf("\n [2].TAMBAH DATA NEGARA");
	                    printf("\n [3].TAMBAH DATA ATLET");
	                    printf("\n [0].MENU UTAMA");
	                    
	                    printf("\n ==================");
	                    printf("\n Masukan Pilihan = "); scanf("%d",&add);
	                    printf("\n ==================");
	                    
	                    switch(add)
						{
	                    	case 1  : 
        	                    	{
        	                    		inputDataCabOr(&list);
        	                    		getch();
        								break;
        							}
        							
							case 2  : 
        							{
										inputDataNegara(&list);
										getch();
        								break;
        							}
        							
							case 3  :
        							{
        								inputDataAtlet(&list);
        								getch();
        								break;
        							}
        							
        					case 0	:
									{
										printf("\nKembali ke menu utama...");
										break;
									}
									
        					default :
        					        {
        					            printf("Masukan tidak valid");
        					        }
						}
					getch();
					}while(add!=0);
					saveFileDataTim(list);
                    break;
                    
            case 2	:
	            	do
					{
						system("color 0f");
	            		system("CLS");
		            	printf("\n Menu UBAH DATA");
		                printf("\n ==============");
		                printf("\n [1]. UBAH CABANG OLAHRAGA");
		                printf("\n [2]. UBAH NEGARA");
		                printf("\n [3]. UBAH ATLET");
		                printf("\n [0]. MENU UTAMA");
		                    
		                printf("\n ==================");
		                printf("\n Masukan Pilihan = "); scanf("%d",&edit);
		                printf("\n ==================\n");
	                    
	                	switch(edit)
						{
		                	case 1  : 
	        	                	{
	        	                		editCabOr(&list);
	        							break;
	        						}
	        						
							case 2  : 
	        						{
	        							editNegara(&list);
	        							break;
	        						}
	        						
							case 3 : 
							        {
	        							editAtlet(&list);
	        							break;
							        }
							        
							case 0	:
									{
										printf("\nKembali ke menu utama...");
										break;
									}
									
							default :
	    					        {
	    					            printf("Menu Hanya 0-3");
	    					        }
						}
					getch();
					}while(edit!=0);
					saveFileDataTim(list);
					break;
                    
            case 3	:
                    do
					{
						system("color 0f");
                    	system("CLS");
                    	printf("\n Menu TAMPIL DATA");
	                    printf("\n ==============");
	                    printf("\n [1]. TAMPIL SEMUA DATA");
	                    printf("\n [2]. TAMPIL DATA ATLET NEGARA TERTENTU");
	                    printf("\n [3]. TAMPIL DATA CABANG OLAHRAGA BERDASAR NEGARA");
	                    printf("\n [0]. MENU UTAMA");
	                    
	                    printf("\n ==================");
	                    printf("\n Masukan Pilihan = "); scanf("%d",&show);
	                    printf("\n ==================\n");
	                    
	                    switch(show)
						{
	                    	case 1  : 
        	                    	{
        	                    		printSemuaData(list);
        	                    		getch();
        								break;
        							}
        							
							case 2  :
        							{
        								printAtletNegaraTertentu(list);
        								getch();
        								break;
        							}
        							
							case 3 :
        							{
        								printCabangOlahragaTertentu(list);
        								getch();
        								break;
        							}
        							
        					case 0	:
									{
										break;
									}
									
        					default :
        					        {
        					            printf("Menu hanya 0-3...");
        					            break;
        					        }
						}
						getch();
					}while(show!=0);
                    break;
                    
            case 4	:
                    do
					{
						system("color 0f");
                    	system("CLS");
                    	printf("\n Menu HAPUS DATA");
	                    printf("\n ==============");
	                    printf("\n [1]. HAPUS NEGARA");
	                    printf("\n [2]. HAPUS ATLET");
	                    printf("\n [0]. MENU UTAMA");
	                    
	                    printf("\n ==================");
	                    printf("\n Masukan Pilihan = "); scanf("%d",&hapus);
	                    printf("\n ==================\n");
	                    
	                    switch(hapus)
						{
	                    	case 1  :
        	                    	{
        	                    		deleteNegara(&list);
        								break;
        							}
        							
							case 2  :
        							{
        								deleteAtlet(&list);
        								break;
        							}
        							
        					case 0	:
									{
										break;
									}
									
        					default :
        					        {
        					            printf("Masukan tidak valid");
        					            break;
        					        }
						}
					getch();
					}while(hapus!=0);
					saveFileDataTim(list);
                    break;
                    
            case 5	:
            		system("color 0f");
            		if(isEmpty)
            		{
            			printf("\nList Masih kosong !");
					}
					else
					{
						inputMedali(&list);
					}
					getch();
					saveFileDataTim(list);
                    break;
                    
            case 6	:
            		system("color 0f");
            		if(isEmpty)
            		{
            			printf("\nList Masih kosong !");
					}
					else
					{
		            	sort(list);
					}
					getch();
					break;
					
            case 0	:
            		system("color 0f");
            		printf("\n\t=== Terima kasih ! Salam Olahraga ! ===");
            		getch();
					break;
            		
            default	:
					{
		            	printf("\nMasukan Tidak Valid!\n");
		            	getch();
						break;
					}
        }
    }while(menu!=0);
	return 0;
}
