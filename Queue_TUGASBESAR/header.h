/*
Anggota Kelompok Queue:
	- Natan Hari Pamungkas 170709254
	- Raditya Dimas Bagus S 170709234
	- Hieronimus Wicaksana G 170709236
	- Rafael Agatha Claudio P 170709132
	- Benedictus Gladdenata A 170709160
*/

// Libraries yang dibutuhkan program
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// ADT pointer multi list
typedef struct tChild* adrChild;
typedef struct tParent* adrParent;
typedef struct tSubChild* adrSubChild;

// ADT pointer singly linked list
typedef struct tMedal* adrMedal;
typedef struct tNegara* adrNegara;

// Record dari atlet
typedef struct{
	char kodeAtlet[4];
	char namaAtlet[45];
	char jenisKelaminAtlet[7];
	int usiaAtlet;
}atlet;

// Node subChild untuk menyimpan atlet
typedef struct tSubChild{
	atlet dataAtlet;
	adrSubChild nextSubChild;	
}subChild;

// Record dari negara
typedef struct{
	char kodeNegara[4];
	char namaNegara[50];
	int medali[3];
}negara;

// Node child untuk menyimpan negara
typedef struct tChild{
	negara dataNegara;
	adrSubChild firstSubChild;
	adrChild nextChild;
}child;

// Record dari cabang olahraga
typedef struct{
	char kodeCabOr[4];
	char namaCabOr[45];	
}cabOr;

// Node parent untuk menyimpan data cabang olahraga
typedef struct tParent{
	cabOr dataCabOr;
	adrParent nextParent;
	adrChild firstChild;
}parent;

// List parent
typedef struct{
	adrParent first;
}listParent;

// Struct untuk node sorting medali
typedef struct tMedal{
	negara dataNegara;
	adrMedal next;
}nodeMedal;

typedef struct tNegara{
	char kodeNegara[4];
	char namaNegara[50];
	adrNegara next;
}nodeNegara;

// Singly list untuk medali
typedef struct{
	adrMedal first;
}listMedal;

typedef struct{
	adrNegara first;
}listNegara;

typedef struct{
	char kodeCabOr[4];
	char kodeNegara[4];
	char namaNegara[50];
	int medali[3];
}tampungNegara;

typedef struct{
	char kodeCabOr[4];
	char kodeNegara[4];
	char kodeAtlet[4];
	char namaAtlet[45];
	char jenisKelaminAtlet[7];
	int usiaAtlet;
}tampungAtlet;

//Prototypes fungsi dan prosedur
void initList(listParent *l);
int isEmpty(listParent l);
int isHaveChild(adrParent p);
int isHaveSubChild(adrChild c);
adrParent alokasiData(cabOr data);
adrParent findParent(listParent l, char kodeCabOr[]);
int isKodeCabOrUnik(listParent l, char kodeCabOr[]);
void insertFirstParent(listParent *l, cabOr data);
void inputDataCabOr(listParent *l);
adrChild alokasiDataChild(negara dataNegara);
void insertFirstChild(adrParent parent, negara dataNegara);
int isKodeNegaraUnik(char kodeNegara[], adrParent parent);
void inputDataNegara(listParent *l);
adrSubChild alokasiSubChild(atlet data);
void insertFirstSubChild(adrChild child, atlet dataAtlet);
int isHaveSubChild(adrChild c);
int isKodeAtletUnik(char kodeAtlet[], adrChild child);
adrChild findChild(adrParent parent, char kodeNegara[]);
void inputDataAtlet(listParent *l);
void editCabOr(listParent *l);
void editNegara(listParent *l);
adrSubChild findSubChild(adrChild child, char kodeAtlet[]);
void editAtlet(listParent *l);
void deleteFirstChild(listParent *l, adrParent parent);
void deleteAtNegara(listParent *l, adrChild child, adrParent parent);
void deleteNegara(listParent *l);
void deleteFirstSubChild(listParent *l, adrChild child);
void deleteAtAtlet(listParent *l, adrChild child, adrSubChild subChild);
void deleteAtlet(listParent *l);
void printSubChild(adrSubChild subChild);
void printChild(adrChild child);
void printSemuaData(listParent l);
void printChildTertentu(adrChild child, char kodeNegara[]);
void printAtletNegaraTertentu(listParent l);
void printCabangOlahragaTertentu(listParent l);
void inputMedali(listParent *l);
void initListMedal(listMedal *l);
adrMedal alokasiMedal(negara dataNegara);
void insertFirstMedal(listMedal *l, adrMedal newNode);
adrMedal findNegaraMedal(listMedal medal, char kodeNegara[4]);
void copyNegara(adrChild child, listMedal *medal);
void loadNegara(listParent l, listMedal *medal);
void tampilMedal(listMedal l);
void sort(listParent l);
void saveFileDataTim(listParent l);
void readDataJuara(listParent *l);
void tampilKodeCabOr(listParent l);
void tampilKodeNegara(adrParent p);
void tampilKodeAtlet(adrChild c);
void insertFirstKodeNegara(listNegara *l, adrNegara newNode);
adrNegara findKodeNegara(listNegara l, char kodeNegara[4]);
adrNegara alokasiKodeNegara(char kodeNegara[], char namaNegara[]);
void initListKodeNegara(listNegara *kodeNegara);
void tampilSemuaKodeNegara(listParent l);
void copyKodeNegara(adrChild child, listNegara *listKodeNegara);
void loadKodeNegara(listParent l, listNegara *listKodeNegara);
