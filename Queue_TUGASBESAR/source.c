#include "header.h"

// Menginisialisasi first parent dengan null
void initList(listParent *l){
	l->first = NULL;
}

//Mengecek apakah list kosong atau tidak
//--> Akan mengembalikan nilai true jika kosong, false jika isi
int isEmpty(listParent l){
	return l.first==NULL;
}

//Mengecek apakah parent memiliki child
//--> Akan mengembalukan nilai true jika punya, false jika tidak
int isHaveChild(adrParent p){
	return p->firstChild!=NULL;
}

//Mengecek apakah child memiliki sub child atau tidak
//--> Akan mengembalukan nilai true jika punya, false jika tidak
int isHaveSubChild(adrChild c){
	return c->firstSubChild!=NULL;
}

//Memesan alamat memori untuk memasukan parent ke dalam list
adrParent alokasiData(cabOr data){
	adrParent p = (adrParent) malloc (sizeof(parent));
	p->dataCabOr = data;
	p->nextParent=NULL;
	p->nextParent = NULL;
	p->firstChild = NULL;
	
	return p;
}

// Mencari node parent berdasarkan kode cabang olahraganya
// Lalu akan mengembalikan alamat memorinya jika ketemu, null jika tidak
adrParent findParent(listParent l, char kodeCabOr[]){
	adrParent p = l.first;
	while(p!=NULL && strcmpi(p->dataCabOr.kodeCabOr, kodeCabOr)!=0){
		p = p->nextParent;
	}
	
	return p;
}

//Mengecek apakah kode cabang olahraga untik atau tidak
//--> Mengembalikan nilai true jika unik, dan false jika tidak
int isKodeCabOrUnik(listParent l, char kodeCabOr[]){
	adrParent p = l.first;
	while(p!=NULL){
		if(strcmpi(p->dataCabOr.kodeCabOr, kodeCabOr)==0){
			return 0;
		}
		p = p->nextParent;
	}
	
	return 1;
}

// Memasukan node parent ke awal list
void insertFirstParent(listParent *l, cabOr data){
	adrParent p;
	p = alokasiData(data);
	p->nextParent = l->first;
	l->first = p;
}

//Prosedur untuk menerima input data cabang olahraga
void inputDataCabOr(listParent *l){
	cabOr data;
	do{
		printf("Masukan 3 Digit Kode Cabang Olahraga: "); fflush(stdin); gets(data.kodeCabOr);
		if((strlen(data.kodeCabOr)!=3) || (!isKodeCabOrUnik(*l, data.kodeCabOr))){
			printf("Kode cabang olahraga harus 3 digit dan unik!\n");
		}
	}while((strlen(data.kodeCabOr)!=3) || !isKodeCabOrUnik(*l, data.kodeCabOr));
		
	do{
		printf("Masukan Nama Cabang Olahraga: "); fflush(stdin); gets(data.namaCabOr);
		if(strlen(data.namaCabOr)==0){
			printf("Nama cabang olahraga tidak boleh kosong!\n");
		}
	}while(strlen(data.namaCabOr)==0);
		
	insertFirstParent(l, data);
		
	printf("Cabang olahraga berhasil diinputkan!\n");
}

//Fungsi untuk memesankan memori untuk data negara
adrChild alokasiDataChild(negara dataNegara){
	adrChild c = (adrChild) malloc(sizeof(child));
	
	c->dataNegara = dataNegara;
	c->nextChild = NULL;
	c->firstSubChild = NULL;
	
	return c;
}

//Memasukan node negara ke dalam list
void insertFirstChild(adrParent parent, negara dataNegara){
	if(parent==NULL){
		printf("Cabang olahraga tidak ada!\n");
	}else{
		adrChild c = alokasiDataChild(dataNegara);
		c->nextChild = parent->firstChild;
		parent->firstChild = c; 
	}
}

//Mengecek apakah kode negara untik atau tidak
//--> Mengembalikan nilai true jika unik, dan false jika tidak
int isKodeNegaraUnik(char kodeNegara[], adrParent parent){
	if(isHaveChild(parent)){
		adrChild c = parent->firstChild;
		while(c!=NULL){
			if(strcmpi(c->dataNegara.kodeNegara, kodeNegara)==0){
				return 0;
			}
			c=c->nextChild;
		}
	}
	return 1;	
}

// Memasukan data negara
void inputDataNegara(listParent *l){
	if(!isEmpty(*l)){
		char kodeCabOr[4];
		tampilKodeCabOr(*l);
		printf("\nMasukan 3 digit kode cabang olahraga: "); fflush(stdin); gets(kodeCabOr);
		adrParent p = findParent(*l, kodeCabOr);
		system("CLS");
		if(p!=NULL){
			negara data;
			do{
				printf("Masukan 3 Digit Kode Negara: "); fflush(stdin); gets(data.kodeNegara);
				if((strlen(data.kodeNegara)!=3) || (!isKodeNegaraUnik(data.kodeNegara, p))){
					printf("Kode negara harus 3 digit dan unik!!\n");
				}
			}while((strlen(data.kodeNegara)!=3) || (!isKodeNegaraUnik(data.kodeNegara, p)));
		
			do{
				printf("Masukan Nama Negara: "); fflush(stdin); gets(data.namaNegara);
				if(strlen(data.namaNegara)==0){
					printf("Nama negara tidak boleh kosong!\n");
				}
			}while(strlen(data.namaNegara)==0);
			int i;
			for(i=0; i<3; i++){
				data.medali[i]=0;
			}
			
			insertFirstChild(p, data);
			printf("Negara berhasil diinputkan!\n");	
		}
	}
}

//Memesankan memori untuk data atlet
adrSubChild alokasiSubChild(atlet data){
	adrSubChild sc = (adrSubChild) malloc(sizeof(subChild));
	sc->dataAtlet = data;
	sc->nextSubChild = NULL;
	
	return sc;
}

// Untuk memasukan atlet ke dalam list
void insertFirstSubChild(adrChild child, atlet dataAtlet){
	if(child==NULL){
		printf("Negara tidak ada!\n");
	}else{
		adrSubChild sc = alokasiSubChild(dataAtlet);
		sc->nextSubChild = child->firstSubChild;
		child->firstSubChild = sc;
	}
}

//Mengecek apakah kode atlet untik atau tidak
//--> Mengembalikan nilai true jika unik, dan false jika tidak
int isKodeAtletUnik(char kodeAtlet[], adrChild child){
	if(isHaveSubChild(child)){
		adrSubChild sc = child->firstSubChild;
		while(sc!=NULL){
			if(strcmpi(sc->dataAtlet.kodeAtlet, kodeAtlet)==0){
				return 0;
			}
			sc=sc->nextSubChild;
		}
	}
	return 1;	
}

// Mencari node child berdasarkan kode negaranya
// Lalu akan mengembalikan alamat memorinya jika ketemu, null jika tidak
adrChild findChild(adrParent parent, char kodeNegara[]){
	adrChild c = parent->firstChild;
	while(c!=NULL && strcmpi(c->dataNegara.kodeNegara, kodeNegara)!=0){
		c = c->nextChild;
	}
	
	return c;
}

// Memasukan data atlet
void inputDataAtlet(listParent *l){
	if(!isEmpty(*l)){
		char kodeCabOr[4];
		tampilKodeCabOr(*l);
		printf("\nMasukan 3 digit kode cabang olahraga: "); fflush(stdin); gets(kodeCabOr);
		adrParent p = findParent(*l, kodeCabOr);
		system("CLS");
		if(p!=NULL){
			tampilKodeNegara(p);
			char kodeNegara[4];
			printf("\nMasukan 3 digit kode Negara: "); fflush(stdin); gets(kodeNegara);
			adrChild c = findChild(p, kodeNegara);
			if(c!=NULL){
				atlet data;
				do{
					printf("Masukan 3 Digit Kode Atlet: "); fflush(stdin); gets(data.kodeAtlet);
					if((strlen(data.kodeAtlet)!=3) || (!isKodeAtletUnik(data.kodeAtlet, c))){
						printf("Kode atlet harus 3 digit dan unik!!\n");
					}
				}while((strlen(data.kodeAtlet)!=3) || (!isKodeAtletUnik(data.kodeAtlet, c)));
			
				do{
					printf("Masukan Nama Atlet: "); fflush(stdin); gets(data.namaAtlet);
					if(strlen(data.namaAtlet)==0){
						printf("Nama atlet tidak boleh kosong!\n");
					}
				}while(strlen(data.namaAtlet)==0);
				
				do{
					printf("Masukan jenis kelasmin (pria/wanita): "); fflush(stdin); gets(data.jenisKelaminAtlet);
				}while(strcmpi(data.jenisKelaminAtlet, "pria")!=0 && strcmpi(data.jenisKelaminAtlet, "wanita")!=0);
				
				do{
					printf("Masukan umur atlet: "); scanf("%d", &data.usiaAtlet);
				}while(data.usiaAtlet<=0);
				
				insertFirstSubChild(c, data);
				printf("Atlet berhasil diinputkan!\n");	
			}
		}
	}
}

// Mencari node sub child berdasarkan kode negaranya
// Lalu akan mengembalikan alamat memorinya jika ketemu, null jika tidak
adrSubChild findSubChild(adrChild child, char kodeAtlet[]){
	adrSubChild sc = child->firstSubChild;
	while(sc!=NULL && strcmpi(sc->dataAtlet.kodeAtlet, kodeAtlet)!=0){
		sc = sc->nextSubChild;
	}
	
	return sc;
}

// Mengubah data pada alamat memori parent yang dicari
void editCabOr(listParent *l){
	if(!isEmpty(*l)){
		char kode[4];
		tampilKodeCabOr(*l);
		printf("\nMasukan kode cabang olahraga yang ingin diubah: "); fflush(stdin); gets(kode);
		
		adrParent parent = findParent(*l, kode);
		
		if(parent!=NULL){
			char cabor[4];
			char nama[45];
			do{
				printf("Masukan 3 Digit Kode Cabang Olahraga: "); fflush(stdin); gets(cabor);
				if((strlen(cabor)!=3) || (!isKodeCabOrUnik(*l, cabor))){
					printf("Kode cabang olahraga harus 3 digit dan unik!\n");
				}
			}while((strlen(cabor)!=3) || (!isKodeCabOrUnik(*l, cabor)));
			strcpy(parent->dataCabOr.kodeCabOr, cabor);
			do{
				printf("Masukan Nama Cabang Olahraga: "); fflush(stdin); gets(nama);
				if(strlen(nama)==0){
					printf("Nama cabang olahraga tidak boleh kosong!\n");
				}
			}while(strlen(nama)==0);
			strcpy(parent->dataCabOr.namaCabOr, nama);
		}
	}
}

//Mengubah data negara yang dicari
void editNegara(listParent *l){
	if(!isEmpty(*l)){
		char kode[4];
		tampilKodeCabOr(*l);
		printf("\nMasukan kode cabang olahraga yang diikuti oleh negara yang ingin diubah: "); fflush(stdin); gets(kode);
		
		adrParent parent = findParent(*l, kode);
		if(parent!=NULL){
			system("CLS");
			char kodeNegara[4];
			tampilKodeNegara(parent);
			printf("\nMasukan kode negara yang ingin diubah: "); fflush(stdin); gets(kodeNegara);
			adrChild child = findChild(parent, kodeNegara);
			
			if(child != NULL){
				do{
					printf("Masukan 3 Digit Kode Negara: "); fflush(stdin); gets(kodeNegara);
					if((strlen(kodeNegara)!=3) || (!isKodeNegaraUnik(kodeNegara, parent))){
						printf("Kode negara harus 3 digit dan unik!!\n");
					}
				}while((strlen(kodeNegara)!=3) || (!isKodeNegaraUnik(kodeNegara, parent)));
				strcpy(child->dataNegara.kodeNegara, kodeNegara);
				char nama[50];
				do{
					printf("Masukan Nama Negara: "); fflush(stdin); gets(nama);
					if(strlen(nama)==0){
						printf("Nama negara tidak boleh kosong!\n");
					}
				}while(strlen(nama)==0);
				strcpy(child->dataNegara.namaNegara, nama);
			}
		}
	}
}

// Mengubah data atlet yang dicari
void editAtlet(listParent *l){
	if(!isEmpty(*l)){
		char kode[4];
		tampilKodeCabOr(*l);
		printf("\nMasukan kode cabang olahraga yang diikuti oleh atlet yang ingin diubah: "); fflush(stdin); gets(kode);
		adrParent parent = findParent(*l, kode);
		if(parent!=NULL){
			system("CLS");
			char kodeNegara[4];
			tampilKodeNegara(parent);
			printf("\nMasukan kode negara asal atlet yang ingin diubah: "); fflush(stdin); gets(kodeNegara);
			adrChild child = findChild(parent, kodeNegara);
			
			if(child!=NULL){
				char kodeAtlet[4];
				system("CLS");
				tampilKodeAtlet(child);
				printf("\nMasukan kode atlet yang akan diubah: "); fflush(stdin); gets(kodeAtlet);
				
				adrSubChild subChild = findSubChild(child, kodeAtlet);
				
				if(subChild!=NULL){
					atlet data;
					do{
						printf("Masukan 3 Digit Kode Atlet: "); fflush(stdin); gets(data.kodeAtlet);
						if((strlen(data.kodeAtlet)!=3) || (!isKodeAtletUnik(data.kodeAtlet, child))){
							printf("Kode atlet harus 3 digit dan unik!!\n");
						}
					}while((strlen(data.kodeAtlet)!=3) || (!isKodeAtletUnik(data.kodeAtlet, child)));
					strcpy(subChild->dataAtlet.kodeAtlet, data.kodeAtlet);
					do{
						printf("Masukan Nama Atlet: "); fflush(stdin); gets(data.namaAtlet);
						if(strlen(data.namaAtlet)==0){
							printf("Nama atlet tidak boleh kosong!\n");
						}
					}while(strlen(data.namaAtlet)==0);
					strcpy(subChild->dataAtlet.namaAtlet, data.namaAtlet);
					do{
						printf("Masukan jenis kelasmin (pria/wanita): "); fflush(stdin); gets(data.jenisKelaminAtlet);
					}while(strcmpi(data.jenisKelaminAtlet, "pria")!=0 && strcmpi(data.jenisKelaminAtlet, "wanita")!=0);
					strcpy(subChild->dataAtlet.jenisKelaminAtlet, data.jenisKelaminAtlet);
					do{
						printf("Masukan umur atlet: "); scanf("%d", &data.usiaAtlet);
					}while(data.usiaAtlet<=0);
					subChild->dataAtlet.usiaAtlet = data.usiaAtlet;
				}
			}
		}
	}
}

// Untuk menghapus node first child dari parent
void deleteFirstChild(listParent *l, adrParent parent){
	adrChild del;
	if(parent!=NULL && isHaveChild(parent)){
		del = parent->firstChild;
		parent->firstChild = parent->firstChild->nextChild;
		free(del);
	}
}

//Menghapus pada alamat negara
void deleteAtNegara(listParent *l, adrChild child, adrParent parent){
	if(!isEmpty(*l)){
		adrChild c;
		if(parent->firstChild==child){
			deleteFirstChild(l, parent);
		}else{
			for(c=parent->firstChild; c->nextChild!=NULL; c = c->nextChild){
				if(c->nextChild==child)
				c->nextChild=child->nextChild;
				free(child);
			}
		}
	}
}

//Menghapus data negara
void deleteNegara(listParent *l){
	if(!isEmpty(*l)){
		char kodeCabOr[4];
		tampilKodeCabOr(*l);
		printf("\nMasukan kode cabang olahraga yang diikuti negara yang ingin dihapus: "); fflush(stdin); gets(kodeCabOr);
		
		adrParent parent = findParent(*l, kodeCabOr);
		
		if(parent!=NULL){
			char kodeNegara[4];
			tampilKodeNegara(parent);
			printf("\nMasukan kode negara yang ingin dihapus: "); fflush(stdin); gets(kodeNegara);
			
			adrChild child = findChild(parent, kodeNegara);
			
			if(child!=NULL){
				deleteAtNegara(l, child, parent);
			}
		}
	}
}

// Untuk menghapus node first sub child dari child
void deleteFirstSubChild(listParent *l, adrChild child){
	adrSubChild del;
	if(child!=NULL && isHaveSubChild(child)){
		del = child->firstSubChild;
		child->firstSubChild = child->firstSubChild->nextSubChild;
		free(del);
	}
}

//Menghapus pada alamat atlet
void deleteAtAtlet(listParent *l, adrChild child, adrSubChild subChild){
	if(!isEmpty(*l)){
		adrSubChild sc;
		if(child->firstSubChild==subChild){
			deleteFirstSubChild(l, child);
		}else{
			for(sc=child->firstSubChild; sc->nextSubChild!=NULL; sc =sc->nextSubChild){
				if(sc->nextSubChild==subChild)
				sc->nextSubChild=subChild->nextSubChild;
				free(subChild);
			}
		}
	}
}

//Menghapus data atlet
void deleteAtlet(listParent *l){
	if(!isEmpty(*l)){
		char kodeCabOr[4];
		tampilKodeCabOr(*l);
		printf("\nMasukan kode cabang olahraga yang diikuti atlet yang ingin dihapus: "); fflush(stdin); gets(kodeCabOr);
		
		adrParent parent = findParent(*l, kodeCabOr);
		
		if(parent!=NULL){
			char kodeNegara[4];
			tampilKodeNegara(parent);
			printf("\nMasukan kode negara asal atlet yang ingin dihapus: "); fflush(stdin); gets(kodeNegara);
			
			adrChild child = findChild(parent, kodeNegara);
			
			if(child!=NULL){
				char kodeAtlet[4];
				tampilKodeAtlet(child);
				printf("\nMasukan kode atlet yang ingin dihapus: "); fflush(stdin); gets(kodeAtlet);
				
				adrSubChild subChild = findSubChild(child, kodeAtlet);
				
				if(subChild!=NULL){
					deleteAtAtlet(l, child, subChild);
				}
			}
		}
	}
}

//Mencetak data atlet
void printSubChild(adrSubChild subChild){
	if(subChild!=NULL){
		while(subChild!=NULL){
			printf("Kode Atlet: %s\n", subChild->dataAtlet.kodeAtlet);
			printf("Nama Atlet: %s\n", subChild->dataAtlet.namaAtlet);
			printf("Jenis Kelamin Atlet: %s\n", subChild->dataAtlet.jenisKelaminAtlet);
			printf("Usia Atlet: %d Tahun\n\n\n", subChild->dataAtlet.usiaAtlet);
			subChild = subChild->nextSubChild;
		}
	}
}

// Mencetak data negara beserta atletnya
void printChild(adrChild child){
	if(child!=NULL){
		while(child!=NULL){
			printf("Kode Negara: %s\n", child->dataNegara.kodeNegara);
			printf("Nama Negara: %s\n\n", child->dataNegara.namaNegara);
			if(isHaveSubChild(child)){
				printf("Atlet-atlet: \n");
				printSubChild(child->firstSubChild);
			}
			child=child->nextChild;
		}
	}
}

// Mencetak data cabang olahraga, negara, dan atletnya
void printSemuaData(listParent l){
	if(!isEmpty(l)){
		adrParent p = l.first;
		while(p!=NULL){
			printf("Kode Cabang Olahraga: %s\n", p->dataCabOr.kodeCabOr);
			printf("Nama Cabang Olahraga: %s\n\n", p->dataCabOr.namaCabOr);
			if(isHaveChild(p)){
				printf("Data Negara: \n");
				printChild(p->firstChild);
			}
			p = p->nextParent;
		}
	}
}

// Untuk mencetak data atlet dari negara tertentu
void printChildTertentu(adrChild child, char kodeNegara[]){
	if(child!=NULL){
		while(child!=NULL){
			if(isHaveSubChild(child)){
				if(strcmpi(child->dataNegara.kodeNegara, kodeNegara)==0){
					printSubChild(child->firstSubChild);
				}
			}
			child = child->nextChild;
		}
	}
}

// Untuk mencetak data atlet dari negara tertentu
void printAtletNegaraTertentu(listParent l){
	if(!isEmpty(l)){
		char kodeNegara[4];
		tampilSemuaKodeNegara(l);
		printf("\nMasukan kode negara: "); fflush(stdin); gets(kodeNegara);
		adrParent p = l.first;
		while(p!=NULL){
			if(isHaveChild(p)){
				printChildTertentu(p->firstChild, kodeNegara);
			}
			p = p->nextParent;
		}
	}
}

// Mencetak data cabang olahraga yang diikuti negara tertentu
void printCabangOlahragaTertentu(listParent l){
	if(!isEmpty(l)){
		adrParent p = l.first;
		char kodeNegara[4];
		tampilSemuaKodeNegara(l);
		printf("\nMasukan kode negara: "); fflush(stdin); gets(kodeNegara);
		while(p!=NULL){
			if(isHaveChild(p)){
				adrChild child = findChild(p, kodeNegara);
				if(child!=NULL){
					printf("Kode Cabang Olahraga: %s\n", p->dataCabOr.kodeCabOr);
					printf("Nama Cabang Olahraga: %s\n\n", p->dataCabOr.namaCabOr);	
				}
			}
			p = p->nextParent;
		}
	}
}

// Prosedur untuk menambahkan medali
void inputMedali(listParent *l){
	if(!isEmpty(*l)){
		char kodeCabOr[4];
		tampilKodeCabOr(*l);
		printf("\nMasukan kode cabang olahraga: "); fflush(stdin); gets(kodeCabOr);
		adrParent parent = findParent(*l, kodeCabOr);
		if(parent!=NULL){
			char kodeNegara[4];
			tampilKodeNegara(parent);
			printf("Masukan kode negara: "); fflush(stdin); gets(kodeNegara);
			adrChild child = findChild(parent, kodeNegara);
			
			if(child!=NULL && isHaveSubChild(child)){
				char jenisMedali[9];
				int jumlahMedali;
				do{
					printf("Masukan medali (emas/perak/perunggu): "); fflush(stdin); gets(jenisMedali);
				}while(strcmpi(jenisMedali, "emas")!=0 && strcmpi(jenisMedali, "perak")!=0 && strcmpi(jenisMedali, "perunggu")!=0);
				do{
					printf("Masukan jumlah medali: "); scanf("%d", &jumlahMedali);
				}while(jumlahMedali<1);
				if(strcmpi(jenisMedali, "emas")==0){
					child->dataNegara.medali[0]+=jumlahMedali;
				}else if(strcmpi(jenisMedali, "perak")==0){
					child->dataNegara.medali[1]+=jumlahMedali;
				}else{
					child->dataNegara.medali[2]+=jumlahMedali;
				}
			}
		}
	}
}

// Untuk inisialisasi list medali
void initListMedal(listMedal *l){
	l->first=NULL;
}

// Untuk alokasi data negara di list medali
adrMedal alokasiMedal(negara dataNegara){
	adrMedal c;
	c=(nodeMedal*)malloc(sizeof(nodeMedal));
	c->dataNegara = dataNegara;
	c->next=NULL;
	
	return c;
}

// Untuk menambahkan negara ke list medali
void insertFirstMedal(listMedal *l, adrMedal newNode){
	newNode->next=l->first;
	l->first=newNode;
}

// Untuk mencari negara pada list medali
adrMedal findNegaraMedal(listMedal medal, char kodeNegara[4]){
	adrMedal c;
	for(c=medal.first; c!=NULL&&strcmpi(c->dataNegara.kodeNegara, kodeNegara)!=0; c=c->next);
	return c;
}

//Untuk menyalin data negara dan dimasukan ke dalam list medali
// Jika negara sudah ada, maka hanya akan menambahkan medalinya
void copyNegara(adrChild child, listMedal *medal){
	if(child!=NULL){
		while(child!=NULL){
			if(isHaveSubChild(child)){
				adrMedal test = findNegaraMedal(*medal, child->dataNegara.kodeNegara);
				if(test!=NULL){
					int i;
					for(i=0; i<3; i++){
						child->dataNegara.medali[i] += test->dataNegara.medali[i];
					}
				}else{
					insertFirstMedal(medal, alokasiMedal(child->dataNegara));
				}
				
			}
			child = child->nextChild;
		}
	}
}

// Untuk menjalankan penyalinan semua negara pada multi list ke list medal
void loadNegara(listParent l, listMedal *medal){
	if(!isEmpty(l)){
		adrParent p = l.first;
		while(p!=NULL){
			if(isHaveChild(p)){
				copyNegara(p->firstChild, medal);
			}
			p = p->nextParent;
		}
	}
}

// Untuk menampilkan juara umum
void tampilMedal(listMedal l){
	adrMedal c;
	for(c=l.first; c!=NULL; c=c->next){
		printf("\nKode Negara: %s\n", c->dataNegara.kodeNegara);
		printf("Nama Negara: %s\n", c->dataNegara.namaNegara);
		printf("Medali Emas: %d\n", c->dataNegara.medali[0]);
		printf("Medali Perak: %d\n", c->dataNegara.medali[1]);
		printf("Medali Perunggu: %d\n", c->dataNegara.medali[2]);
		int i, jumlah=0;
		for(i=0; i<3; i++){
			jumlah+=c->dataNegara.medali[i];
		}
		printf("Total Medali: %d\n\n\n", jumlah);
	}
}

// Untuk mengurutkan secara descending sehingga negara paling banyak memperoleh medali akan menjadi juara umum
void sort(listParent l){
	if(!isEmpty(l)){
		listMedal medal;
		initListMedal(&medal);
		loadNegara(l, &medal);
		int kondisi;
		adrMedal c, check=NULL;
		do{
			kondisi=0;
			for(c=medal.first; c->next!=check; c=c->next){
				int jumlah1=0, jumlah2=0, i;
				for(i=0; i<3; i++){
					jumlah1+=c->dataNegara.medali[i];
					jumlah2+=c->next->dataNegara.medali[i];
				}
				if(jumlah1<jumlah2){
					negara tampung = c->dataNegara;
					c->dataNegara = c->next->dataNegara;
					c->next->dataNegara=tampung;
					kondisi=1;
				}
			}
			check=c;
		}while(kondisi);
		tampilMedal(medal);
	}
}

void saveFileDataTim(listParent l){
	FILE *dataFile, *negara, *atlet;
	remove("DataCabOr.bin");
	remove("negara.bin");
	remove("atlet.bin");
	dataFile = fopen("DataCabOr.bin", "wb");
	adrParent p = l.first;
	cabOr tampungCabOr;
	tampungNegara tempNegara;
	tampungAtlet tempAtlet;
	while(p!=NULL){
		tampungCabOr = p->dataCabOr;
		fwrite(&tampungCabOr, sizeof(cabOr), 1, dataFile);
		adrChild c = p->firstChild;
		while(c!=NULL){
			
			negara = fopen("negara.bin", "ab");
			strcpy(tempNegara.kodeCabOr, p->dataCabOr.kodeCabOr);
			strcpy(tempNegara.kodeNegara, c->dataNegara.kodeNegara);
			strcpy(tempNegara.namaNegara, c->dataNegara.namaNegara);
			int i;
			for(i=0; i<3; i++){
				tempNegara.medali[i] = c->dataNegara.medali[i];
			}
			fwrite(&tempNegara, sizeof(tampungNegara), 1, negara);
			fclose(negara);
			adrSubChild sc = c->firstSubChild;
			while(sc != NULL){
				
				atlet = fopen("atlet.bin", "ab");
				strcpy(tempAtlet.kodeCabOr, p->dataCabOr.kodeCabOr);
				strcpy(tempAtlet.kodeNegara, c->dataNegara.kodeNegara);
				strcpy(tempAtlet.kodeAtlet, sc->dataAtlet.kodeAtlet);
				strcpy(tempAtlet.namaAtlet, sc->dataAtlet.namaAtlet);
				tempAtlet.usiaAtlet=sc->dataAtlet.usiaAtlet;
				strcpy(tempAtlet.jenisKelaminAtlet, sc->dataAtlet.jenisKelaminAtlet);
				fwrite(&tempAtlet, sizeof(tampungAtlet), 1, atlet);
				fclose(atlet);
				sc = sc->nextSubChild;
			}
			c = c->nextChild;
		}
		p= p->nextParent;
	}
	fclose(dataFile);
}

void readDataJuara(listParent *l){
	FILE *dataFile;
	cabOr tampungCabOr;
	dataFile=fopen("DataCabOr.bin", "rb");
	while(fread(&tampungCabOr, sizeof(cabOr), 1, dataFile)){
		adrParent cariP = findParent(*l, tampungCabOr.kodeCabOr);
		if(cariP == NULL){
			insertFirstParent(l, tampungCabOr);
		}
	}
	fclose(dataFile);
	
	FILE *fileNegara;
	tampungNegara tempNegara;
	fileNegara=fopen("negara.bin", "rb");
	while(fread(&tempNegara, sizeof(tampungNegara), 1, fileNegara)){
		adrParent p = findParent(*l, tempNegara.kodeCabOr);
		if(p!=NULL){
			negara dataNegara;
			strcpy(dataNegara.kodeNegara, tempNegara.kodeNegara);
			strcpy(dataNegara.namaNegara, tempNegara.namaNegara);
			int i;
			for(i=0; i<3; i++){
				dataNegara.medali[i] = tempNegara.medali[i];
			}
			adrChild checkC = findChild(p, tempNegara.kodeNegara);
			if(checkC == NULL){
				insertFirstChild(p, dataNegara);
			}
		}
	}
	fclose(fileNegara);
	
	FILE *fileAtlet;
	tampungAtlet tempAtlet;
	fileAtlet=fopen("atlet.bin", "rb");
	while(fread(&tempAtlet, sizeof(tampungAtlet), 1, fileAtlet)){
		adrParent p = findParent(*l, tempAtlet.kodeCabOr);
		if(p!=NULL){
			adrChild c = findChild(p, tempAtlet.kodeNegara);
			if(c!=NULL){
				atlet dataAtlet;
				strcpy(dataAtlet.kodeAtlet, tempAtlet.kodeAtlet);
				strcpy(dataAtlet.namaAtlet, tempAtlet.namaAtlet);
				strcpy(dataAtlet.jenisKelaminAtlet, tempAtlet.jenisKelaminAtlet);
				dataAtlet.usiaAtlet = tempAtlet.usiaAtlet;
				adrSubChild cariSc = findSubChild(c, tempAtlet.kodeAtlet);
				if(cariSc == NULL){
					insertFirstSubChild(c, dataAtlet);
				}
			}
		}
	}
	fclose(fileAtlet);
}

void tampilKodeCabOr(listParent l){
	if(!isEmpty(l)){
		adrParent p = l.first;
		int i=0;
		while(p!=NULL){
			printf("%s (%s) ", p->dataCabOr.namaCabOr, p->dataCabOr.kodeCabOr);
			i++;
			if(i==5){
				printf("\n");
				i=0;
			}
			p = p->nextParent;
		}
	}
}

void tampilKodeNegara(adrParent p){
	if(p!=NULL){
		adrChild c = p->firstChild;
		int i=0;
		while(c!=NULL){
			printf("%s (%s) ", c->dataNegara.namaNegara, c->dataNegara.kodeNegara);
			i++;
			if(i==5){
				printf("\n");
				i=0;
			}
			c = c->nextChild;
		}
	}
}

void tampilKodeAtlet(adrChild c){
	if(c!=NULL){
		adrSubChild sc = c->firstSubChild;
		int i=0;
		while(sc!=NULL){
			printf("%s (%s) ", sc->dataAtlet.namaAtlet, sc->dataAtlet.kodeAtlet);
			i++;
			if(i==5){
				printf("\n");
				i=0;
			}
			sc = sc->nextSubChild;
		}
	}
}

// Untuk menambahkan kodeNegara ke list negara
void insertFirstKodeNegara(listNegara *l, adrNegara newNode){
	newNode->next=l->first;
	l->first=newNode;
}

// Untuk mencari kode negara pada list negara
adrNegara findKodeNegara(listNegara l, char kodeNegara[4]){
	adrNegara c;
	for(c=l.first; c!=NULL&&strcmpi(c->kodeNegara, kodeNegara)!=0; c=c->next);
	return c;
}

// Untuk alokasi data kode negara di list negara
adrNegara alokasiKodeNegara(char kodeNegara[], char namaNegara[]){
	adrNegara c;
	c=(nodeNegara*)malloc(sizeof(nodeNegara));
	strcpy(c->kodeNegara, kodeNegara);
	strcpy(c->namaNegara, namaNegara);
	c->next=NULL;
	
	return c;
}

void initListKodeNegara(listNegara *kodeNegara){
	kodeNegara->first=NULL;
}

//Untuk menyalin data negara dan dimasukan ke dalam list negara
void copyKodeNegara(adrChild child, listNegara *listKodeNegara){
	if(child!=NULL){
		while(child!=NULL){
			adrNegara dataKode = findKodeNegara(*listKodeNegara, child->dataNegara.kodeNegara);
			
			if(dataKode==NULL){
				insertFirstKodeNegara(listKodeNegara, alokasiKodeNegara(child->dataNegara.kodeNegara, child->dataNegara.namaNegara));
			}
			
			child = child->nextChild;
		}
	}
}

// Untuk menjalankan penyalinan semua negara pada multi list ke list negara
void loadKodeNegara(listParent l, listNegara *listKodeNegara){
	if(!isEmpty(l)){
		adrParent p = l.first;
		while(p!=NULL){
			if(isHaveChild(p)){
				copyKodeNegara(p->firstChild, listKodeNegara);
			}
			p = p->nextParent;
		}
	}
}

void tampilSemuaKodeNegara(listParent l){
	listNegara list;
	initListKodeNegara(&list);
	
	loadKodeNegara(l, &list);
	
	adrNegara data = list.first;
	
	int i=0;
	while(data!=NULL){
		printf("%s (%s) ", data->namaNegara, data->kodeNegara);
		i++;
		if(i==5){
			printf("\n");
			i=0;
		}
		data = data->next;
	}
}
